#!/usr/bin/env python3

import os
import sys

sys.path.append(os.path.dirname(__file__))

import rtool.main

if __name__ == "__main__":
    rtool.main.main()
