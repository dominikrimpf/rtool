import argparse
import os.path
import sys

class Arguments(object):
    def __init__(self):
        parser = argparse.ArgumentParser(
            description='Registrar tool',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

        parser.add_argument('--config', '-c',
            default=os.path.join(os.path.expanduser('~'), '.config','rtool','rtool.yml'),
            help='User configuration file')
        parser.add_argument('--url', '-u',
            help='API URL')
        parser.add_argument('--token', '-t',
            help='API authentication token')
        parser.add_argument('--transaction', '-x',
            help='Transaction id')
        parser.add_argument('--account', '-a',
            help='Account id')
        parser.add_argument('--verbose', '-v',
            default=0,
            action='count',
            help='set verbosity level')
        parser.add_argument('--output', '-o',
            default='default',
            choices=['yaml','json','pprint'],
            help='output format')

        serviceparsers = parser.add_subparsers()
        serviceparsers.required=True

        parser_domain = serviceparsers.add_parser('domain', help='maintain domains')
        parser_domain.set_defaults(service='domain')

        methodparsers_domain = parser_domain.add_subparsers()
        methodparsers_domain.required=True


        parser_domain_list = methodparsers_domain.add_parser('list',
            help='list registered domains')
        parser_domain_list.add_argument('--contact',
            help='contact id or handle filter')
        parser_domain_list.set_defaults(method='list')

        parser_domain_show = methodparsers_domain.add_parser('show',
            help='show details of domains')
        parser_domain_show.set_defaults(method='show')
        parser_domain_show.add_argument('domain',
            metavar='domain.tld',
            help='show domains')

        parser_domain_check = methodparsers_domain.add_parser('check',
            help='check domain name availability')
        parser_domain_check.set_defaults(method='check')
        parser_domain_check.add_argument('domain',
            metavar='domain.tld',
            nargs='+',
            help='domains to check')

        parser_domain_price = methodparsers_domain.add_parser('price',
            help='show tld price')
        parser_domain_price.set_defaults(method='price')
        parser_domain_price.add_argument('tld',
            nargs='*',
            help='filter for top level domain')

        parser_domain_register = methodparsers_domain.add_parser('register',
            help='register or transfer domain')
        parser_domain_register.set_defaults(method='register')
        parser_domain_register.add_argument('domain',
            metavar='domain.tld',
            help='domain')
        parser_domain_register.add_argument('--owner',
            metavar='contact',
            help='owner contact id or handle')
        parser_domain_register.add_argument('--admin',
            metavar='contact',
            help='admin contact id or handle')
        parser_domain_register.add_argument('--tech',
            metavar='contact',
            help='tech contact id or handle')
        parser_domain_register.add_argument('--zone',
            metavar='contact',
            help='zone contact id or handle')
        parser_domain_register.add_argument('--transfer',
            metavar='auth_info',
            help='transfer domain')
        parser_domain_register.add_argument('--nameserver',
            action='append',
            default=[],
            help='nameserver for this domain')
        parser_domain_register.add_argument('--key',
            metavar=('flags', 'protocol', 'algorithm', 'key'),
            nargs=4,
            action='append',
            default=[],
            help='dnskey for this domain')
        parser_domain_register.add_argument('--yes',
            default=False,
            action='store_true',
            help='alway anser questions with yes')
        parser_domain_register.add_argument('--quiet',
            default=False,
            action='store_true',
            help='suppress extra output')

        parser_domain_update = methodparsers_domain.add_parser('update',
            help='update domain')
        parser_domain_update.set_defaults(method='update')
        parser_domain_update.add_argument('domain',
            metavar='domain.tld',
            help='domain')
        parser_domain_update.add_argument('--owner',
            metavar='contact',
            help='owner contact id or handle')
        parser_domain_update.add_argument('--admin',
            metavar='contact',
            help='admin contact id or handle')
        parser_domain_update.add_argument('--tech',
            metavar='contact',
            help='tech contact id or handle')
        parser_domain_update.add_argument('--zone',
            metavar='contact',
            help='zone contact id or handle')
        parser_domain_update.add_argument('--nameserver',
            action='append',
            default=[],
            help='nameserver for this domain')
        parser_domain_update.add_argument('--newkey',
            metavar=('flags', 'protocol', 'algorithm', 'key'),
            nargs=4,
            action='append',
            default=[],
            help='add new key')
        parser_domain_update.add_argument('--removekey',
            metavar='tag',
            action='append',
            default=[],
            help='remove old key by tag')

        parser_domain_jobs = methodparsers_domain.add_parser('jobs',
            help='list jobs')
        parser_domain_jobs.set_defaults(method='jobs')
        parser_domain_jobs.add_argument('--sid',
            metavar='ID',
            help='search by serverTransactionId')
        parser_domain_jobs.add_argument('--cid',
            metavar='ID',
            help='search by clientTransactionId')
        parser_domain_jobs.add_argument('--failed',
            action='store_true',
            help='filter for failed jobs')
        parser_domain_jobs.add_argument('--running',
            action='store_true',
            help='filter for running')

        parser_contact = serviceparsers.add_parser('contact', help='maintain contacts')
        parser_contact.set_defaults(service='contact')

        methodparsers_contact = parser_contact.add_subparsers()
        methodparsers_contact.required=True

        parser_contact_list = methodparsers_contact.add_parser('list',
            help='list contacts')
        parser_contact_list.set_defaults(method='list')
        parser_contact_list.add_argument('--hidden',
            action='store_true',
            help='list hidden contacts')

        parser_contact_show = methodparsers_contact.add_parser('show',
            help='show contact details')
        parser_contact_show.set_defaults(method='show')
        parser_contact_show.add_argument('contact',
            help='contact id or handle')

        parser_contact_create = methodparsers_contact.add_parser('create',
            help='create contact')
        parser_contact_create.set_defaults(method='create')
        parser_contact_create.add_argument('--type',
            choices=['person','org','role'],
            help='contact type')
        parser_contact_create.add_argument('--name',
            help='contact name')
        parser_contact_create.add_argument('--organization',
            help='organization')
        parser_contact_create.add_argument('--street',
            action='append',
            help='street name')
        parser_contact_create.add_argument('--postalCode',
            help='postal code')
        parser_contact_create.add_argument('--city',
            help='city')
        parser_contact_create.add_argument('--country',
            help='country')
        parser_contact_create.add_argument('--emailAddress',
            help='email address')
        parser_contact_create.add_argument('--phoneNumber',
            help='phone number')
        parser_contact_create.add_argument('--faxNumber',
            help='fax number')

        parser_contact_update = methodparsers_contact.add_parser('update',
            help='update contact')
        parser_contact_update.set_defaults(method='update')
        parser_contact_update.add_argument('--street',
            action='append',
            help='street name')
        parser_contact_update.add_argument('--postalCode',
            help='postal code')
        parser_contact_update.add_argument('--city',
            help='city')
        parser_contact_update.add_argument('--country',
            help='country')
        parser_contact_update.add_argument('--emailAddress',
            help='email address')
        parser_contact_update.add_argument('--phoneNumber',
            help='phone number')
        parser_contact_update.add_argument('--faxNumber',
            help='fax number')
        parser_contact_update.add_argument('contact',
            help='contact id or handle')

        parser_contact_delete = methodparsers_contact.add_parser('delete',
            help='delete contact', epilog='NOTE from API-Documentation: Deleting contacts via API is not possible at the moment and might never be. Unused contacts will automatically be deleted after a while. In that case, an automatic poll message will inform you.')
        parser_contact_delete.set_defaults(method='delete')
        parser_contact_delete.add_argument('contact',
            help='contact id or handle')

        parser_zone = serviceparsers.add_parser('zone', help='maintain zones')
        parser_zone.set_defaults(service='zone')

        methodparsers_zone = parser_zone.add_subparsers()
        methodparsers_zone.required=True

        parser_zone_list = methodparsers_zone.add_parser('list',
            help='list zones')
        parser_zone_list.set_defaults(method='list')

        parser_zone_show = methodparsers_zone.add_parser('show',
            help='show details of zone')
        parser_zone_show.set_defaults(method='show')
        parser_zone_show.add_argument('domain',
            metavar='domain.tld',
            help='zone')

        parser_zone_create = methodparsers_zone.add_parser('create',
            help='create zone')
        parser_zone_create.set_defaults(method='create')
        parser_zone_create.add_argument('--master',
            help='master nameserver')
        parser_zone_create.add_argument('domain',
            metavar='domain.tld',
            help='zone')

        parser_zone_update = methodparsers_zone.add_parser('update',
            help='update zone')
        parser_zone_update.set_defaults(method='update')
        parser_zone_update.add_argument('--master',
            help='master nameserver')
        parser_zone_update.add_argument('domain',
            metavar='domain.tld',
            help='zone')

        parser_zone_delete = methodparsers_zone.add_parser('delete',
            help='delete zone')
        parser_zone_delete.set_defaults(method='delete')
        parser_zone_delete.add_argument('domain',
            metavar='domain.tld',
            help='zone')

        parser_zone_jobs = methodparsers_zone.add_parser('jobs',
            help='list jobs')
        parser_zone_jobs.set_defaults(method='jobs')
        parser_zone_jobs.add_argument('--sid',
            metavar='ID',
            help='search by serverTransactionId')
        parser_zone_jobs.add_argument('--cid',
            metavar='ID',
            help='search by clientTransactionId')
        parser_zone_jobs.add_argument('--failed',
            action='store_true',
            help='filter for failed jobs')
        parser_zone_jobs.add_argument('--running',
            action='store_true',
            help='filter for running jobs')

        parser_raw = serviceparsers.add_parser('raw', help='api raw command')
        parser_raw.set_defaults(service='raw')

        parser_raw.add_argument('--lazy',
            action='store_true',
            default=False,
            help='allow --token and --transaction')
        parser_raw.add_argument('api_service',
            help='api service')
        parser_raw.add_argument('api_method',
            help='api method')
        parser_raw.add_argument('json',
            nargs='?',
            help='json payload')

        try:
           self.arguments  = parser.parse_args()
        except TypeError:
           parser.print_help()
           print('service or method missing?')
           sys.exit(1)
