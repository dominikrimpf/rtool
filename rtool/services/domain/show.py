from ..raw import base

class Method(base.Method):
    def service(self):
        return 'domain'

    def method(self):
        return 'domainInfo'

    def data(self):
        data = {}
        data['domainName'] = self.args.domain
        return data

    def format_default_success(self, dct):
        x = {}
        for k in ['contacts', 'dnsSecEntries', 'nameservers']:
            if k == 'nameservers':
               x[k] = []
               for n in dct['response'][k]:
                   x[k].append('{}{}'.format(n['name'], " ".join(['']+n['ips'])))
            elif k == 'dnsSecEntries':
                x[k] = []
                for n in dct['response'][k]:
                    key = '{flags} {protocol} {algorithm} {publicKey}'.format(**n['keyData'])
                    tag = n['keyTag']
                    x[k].append({'key':key,'tag':tag})
            elif k == 'contacts':
                contacts = {}
                for c in dct['response'][k]:
                    contacts[c['type']] = c['contact']
                x[k] = []
                for t in ['owner', 'admin', 'tech', 'zone']:
                    from .. import contact
                    self.args.contact = contacts[t]
                    y = contact.show.Method(self.config, self.args, self.session)
                    z = y.f_contact(y.request()['response'])
                    del self.args.contact
                    x[k].append({'{}-c'.format(t):z})
            else:
                x[k] = dct['response'][k]
        domain = {dct['response']['nameUnicode']:x}
        return self.format_yaml(domain)
