from ..raw import base

class Method(base.Method):
    def service(self):
        return 'domain'

    def method(self):
        return 'jobsFind'

    def data(self):
        data = {'limit':'0'}
        filter = []
        if 'sid' in self.args and self.args.sid is not None:
            filter.append(('JobServerTransactionId', self.args.sid))
        if 'cid' in self.args and self.args.cid is not None:
            filter.append(('JobClientTransactionId', self.args.cid))
        if 'failed' in self.args and self.args.failed:
            filter.append(('JobState', 'failed'))
        if 'running' in self.args and self.args.running:
            filter.append(('JobState', 'inProgress'))
        data.update(self.filter(*filter))
        return data

    def format_default_success(self, dct):
        return self.format_yaml(dct['response']['data'])
