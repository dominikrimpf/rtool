from . import update
import sys

class Method(update.Method):
    def data(self):
        from .. import domain
        domains = domain.list.Method(self.config, self.args, self.session).request()['response']['data']
        if domains:
            print('Handle is in use by')
            print(self.format_yaml([ d['nameUnicode'] for d in domains ]))
            print('Aborting')
            sys.exit(1)
        data = super(Method, self).data()
        data['contact']['hidden'] = True
        return data

    def format_default_success(self, dct):
        return self.format_yaml(self.f_contact(dct['response'],['hidden']))
