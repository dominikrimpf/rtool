from . import list

class Method(list.Method):
    def method(self):
        return 'contactInfo'

    def data(self):
        data = {}
        data['contactId'] = self.args.contact
        return data

    def format_default_success(self, dct):
        return self.format_yaml(self.f_contact(dct['response']))
