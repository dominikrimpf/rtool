import os
import glob
__all__ = [x for x in [os.path.basename(os.path.normpath(d)) for  d in glob.glob(os.path.dirname(__file__)+'/*/')] if not x.startswith('_')]

from . import *

def get(service):
     try:
        return globals()[service]
     except KeyError:
        raise NotImplementedError("service {service} not implemented yet".format(service=service))
