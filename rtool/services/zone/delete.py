from ..raw import base

class Method(base.Method):
    def service(self):
        return 'dns'

    def method(self):
        return 'zoneDelete'

    def data(self):
        data = {'zoneName':self.args.domain}
        return data

    def format_default_success(self, dct):
         message = []
         message.append("zone: {}".format(self.args.domain))
         message.append("status: {}".format(dct['status']))
         return "\n".join(message)
