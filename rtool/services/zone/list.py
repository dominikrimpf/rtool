from ..raw import base

class Method(base.Method):
    def service(self):
        return 'dns'

    def method(self):
        return 'zoneConfigsFind'

    def data(self):
        data = {}
        data['limit'] = 0
        data['sort'] = {"field": "ZoneNameUnicode", "order": "asc"}
        return data

    def format_default_success(self, dct):
        zones = []
        for zoneconfigobj in dct['response']['data']:
            zones.append(zoneconfigobj['nameUnicode'])
        return "\n".join(zones)
