from ..raw import base

class Method(base.Method):
    def service(self):
        return 'dns'

    def method(self):
        return 'zoneCreate'

    def data(self):
        data = {}
        data['records'] = []
        data['zoneConfig'] = {}
        data['zoneConfig']['name'] = self.args.domain
        data['zoneConfig']['type'] = 'SLAVE'
        if 'master' in self.args and self.args.master is not None:
            data['zoneConfig']['masterIp'] = self.args.master
        elif 'zone' in self.config and 'master' in self.config['zone']:
            data['zoneConfig']['masterIp'] = self.config['zone']['master']
        return data

    def format_default_success(self, dct):
         message = []
         message.append("zone: {}".format(dct['response']['zoneConfig']['name']))
         message.append("master: {}".format(dct['response']['zoneConfig']['masterIp']))
         message.append("status: {}".format(dct['status']))
         return "\n".join(message)
