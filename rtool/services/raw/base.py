import pprint
import yaml
import json
import inspect
import sys
import os

class Method(object):
    def __init__(self, config, args, session):
        self.config = config
        self.args = args
        self.session = session

    def execute(self):
        result = self.request()
        self.print_result(result)

    def print_result(self, result):
        methods = inspect.getmembers(self, predicate=inspect.ismethod)
        format = dict([ (f[0].split('_', 1)[1], f[1]) for f in methods
            if f[0].startswith('format_') ])

        if self.args.output in format:
            print(format[self.args.output](result))
        else:
            raise NotImplementedError('output format {output} not supported'.format(output=self.args.output))

    def format_default(self, dct):
        func = {}
        func['success'] = self.format_default_success
        func['pending'] = self.format_default_pending
        func['error'] = self.format_default_error
        if dct['status'] in func:
            return func[dct['status']](dct)
        else:
            raise NotImplementedError("result status {status} unknown. serverTransactionId: {id}".format(status=dct['status'], id=dct['metadata']['serverTransactionId']))

    def format_default_success(self, dct):
        message = []
        message.append(self.format_yaml(dct))
        if dct['warnings']:
            message.append('There are warnings. Pleas check message above.')
        return '\n'.join(message)

    def format_default_pending(self, dct):
        message = []
        message.append(self.format_default_success(dct))
        message.append("Job is pending. Check result with {tool} {service} jobs --sid '{sid}'".format(tool=os.path.basename(sys.argv[0]),service=self.args.service,sid=dct['metadata']['serverTransactionId']))
        return "\n".join(message)

    def format_default_error(self, dct):
        message = []
        message.append(self.format_yaml(dct))
        message.append('There was an error. Pleas check message above.')
        return "\n".join(message)

    def format_yaml(self, dct):
        return yaml.dump(dct, default_flow_style=False)

    def format_json(self, dct):
        return json.dumps(dct, indent=4, sort_keys=True)

    def format_pprint(self, dct):
        return pprint.pformat(dct)

    def request(self):
        result =  self.session.request(self.service(), self.method(), self.data(), self.transaction_id())
        if not result.ok:
            pprint.pprint(result.text)
            result.raise_for_status()
        else:
            return result.json()

    def transaction_id(self):
        if 'transaction' in self.args and self.args.transaction is not None:
            return self.args.transaction
        elif 'api' in self.config and 'transcation' in self.config['api']:
            return self.config['api']['transaction']
        else:
            return None

    def service(self):
        raise NotImplementedError

    def method(self):
        raise NotImplementedError

    def data(self):
        raise NotImplementedError

    def filter(self, *flist, connective='OR'):
        fx = []
        for f in flist:
            if len(f) == 1:
               raise ValueError
            if len(f) == 2:
               fx.append({'field':f[0], 'value':f[1]})
            elif len(f) == 3:
               fx.append( {'field':f[0], 'relation':f[2], 'value':f[3]})
        if 	len(fx) == 1:
            return {'filter': fx[0] }
        elif 	len(fx) >= 1:
            return {'filter': {'subFilterConnective': connective, 'subFilter': fx}}
        else:
            return {}
